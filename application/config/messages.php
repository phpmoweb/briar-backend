<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/* ========= Alert messages of admin panel ======== */
$config = array(
    "login_error" => "Incorrect Email address or Password!",
    "login_error_blank" => "Please enter your Email address & Password.",
    "logout_success" => "Logout Successful",
    "logout_failed" => "You can't ablt to Logout.",
    "profile_update" => "Your profile has been updated successfully.",
    "user_email_exist" => "This email address already exists!",
    "user_update" => "Details are updated successfully.",
    "approve_user" => "User re-activated successfully.",
    "inactive_user" => 'User de-activated successfully.',
    "record_delete" => "Record deleted successfully.",
    "record_updated" => "Record updated successfully.",
    "user_delete" => "User account deleted successfully.",
    "account_not_activate" => "Your account has not yet been activated for login. Please try again later.",
    /* ==========Api Messages============= */
    "parameter_missing" => "Parameter missing",
    "password_changed" => "Password changed successfully.",
    "password_not_changed" => "Password not changed.",
    "old_password_not_match" => "Old password not matched.",
    "email_not_registered" => "This email is not registered.",
    "email_registered" => "This email is already registered with us.",
    "userName_registered" => "This UserName is already registered with us.",
    "userName_not_registered" => "This UserName is not registered.",
    "register_sucess" => "Registered successfully.",
    "email_sent" => "Email sent successfully.",
    "email_not_sent" => "Email not sent successfully.",
    "no_record_found" => "No records found.",
    "email_pass_missing" => "EmailId or Password is missing.",
    "pass_incorrect" => "Password is incorrect.",
    "login_success" => "Login Successfully.",
    "not_inserted" => "Not Inserted.",
    "posted_success" => "Posted successfully.",
    "profile_success" => "Profile updated successfully",
    "request_success" => "Your Request send Successfully.",
    "failed_update_profile" => "Failed to update profile.",
    "user_report_success" => "User Reported successfully.",
    "failed_to_report" => "Failed to report user.",
    "notification_sent" => "Notification sent successfully.",
    /* =========== Mail subject ============ */
    "forgot_password" => APP_NAME . " | Reset Your Password",
    "new_customer_register" => APP_NAME . " | Welcome Email",
);

