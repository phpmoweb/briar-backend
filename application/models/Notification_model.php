<?php

require_once(APPPATH . '/libraries/Common_Model.php');

class Notification_model extends Common_Model {

    function __construct() {
        parent::__construct();
        $this->tableName = 'tbl_notifications';
        $this->key = 'id';
        $this->fields = "id,userId,deviceType,message,createdDate,status";
    }

    function addRecords($data_to_store) {
        $addRecord = $this->addRecordsCommon($this->tableName, $data_to_store);
        return $addRecord;
    }

    function updateRecords($data_to_update, $key, $value) {
        $updateRecord = $this->updateRecordsCommon($this->tableName, $data_to_update, $key, $value);
        return $updateRecord;
    }

    function getListByIdForOneRecord($key, $value) {
        $recordById = $this->getListByIdForOneRecordCommon($this->tableName, $key, $value, $this->fields);
        return $recordById;
    }

    function getListByIdForAllRecords($key, $value) {
        $recordById = $this->getListByIdForAllRecordsCommon($this->tableName, $key, $value, $this->fields);
        return $recordById;
    }

    function getListByTwoKeysForAllRecords($key1, $value1, $key2, $value2) {
        $recordById = $this->getListByTwoKeysForAllRecordsCommon($this->tableName, $key1, $value1, $key2, $value2, $this->fields);
        return $recordById;
    }

    function deleteAll($ids, $key) {
        $recordById = $this->deleteAllCommon($ids, $this->tableName, $key);
        return $recordById;
    }

    function getDevices($os) {
        $this->db->select('d.deviceId,d.userId,d.deviceType,d.deviceToken');
        $this->db->from('tbl_devices d');
        if ($os != "Both") {
            $this->db->where('d.deviceType', $os);
        }
        $this->db->where('d.isLogin', '1');
        $this->db->where('d.userId != ""');
        $this->db->group_by('d.deviceToken');
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
    }

    function sendNotification($deviceType, $notification_text, $device_token) {

        $android_devicetoken[] = $device_token;

        $desc = $notification_text;

        // Set POST variables 

        $url = 'https://fcm.googleapis.com/fcm/send';


        $message = array("message" => $desc);

        //echo $totalNotifications;
        $notificationArray = array(
            'badge' => '1', //Number to be displayed in the top right of your app icon this should not be a string 
            //'badge' => $totalNotifications, //Number to be displayed in the top right of your app icon this should not be a string 
            'body' => $desc,
            'sound' => 'default',
            'title' => 'Fremantle Markets',
        );

        if ($deviceType == 'Iphone') {
            $fields = array(
                'registration_ids' => $android_devicetoken,
                'data' => $message,
                'notification' => $notificationArray,
            );
        } else {
            $fields = array(
                'registration_ids' => $android_devicetoken,
                'data' => $message,
            );
        }

        $API_KEY = GOOGLE_API_KEY;

        $headers = array(
            'Authorization: key=' . $API_KEY,
            'Content-Type: application/json'
        );

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_POST, true);

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        $result = curl_exec($ch);

        if ($result === FALSE) {

            die('Curl failed: ' . curl_error($ch));
        }

        curl_close($ch);

        return $result;
    }

}
