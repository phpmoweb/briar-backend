<?php

require_once(APPPATH . '/libraries/Common_Model.php');

class Documents_model extends Common_Model {

    function __construct() {
        parent::__construct();
        $this->tableName = 'tbl_documents';
        $this->key = 'id';
        $this->fields = "id,doc_for,title,document,type,createdDate,status";
    }

    //    get document data at ajax call
    function get_document_datatables($searchWhere = "", $searchParamVal = "", $start = "", $length = "", $orderByKey = "", $orderByVal = "", $joinData = "") {

        $selecFileds = $this->fields;
        if (!empty($joinData['select'])) {
            $selecFileds = $this->fields . ", " . $joinData['select'];
        }

        //define columns for searching
        $aColumns = explode(",", $selecFileds);

        // Addtitional Filtering
        $sWhere = "";
        if ($searchParamVal != "") {
            $sWhere .= "(";
            for ($i = 0; $i < count($aColumns); $i++) {
                $sWhere .= $aColumns[$i] . " LIKE '%" . $searchParamVal . "%' OR ";
            }
            $sWhere = substr_replace($sWhere, "", -3);
            $sWhere .= ')';
        }

        if (!empty($orderByKey)) {
            $this->key = $orderByKey;
        }

        $newResult = $this->get_datatables($this->tableName, $selecFileds, $this->key, $orderByVal, $searchWhere, $sWhere, $start, $length, $joinData);
        return $newResult;
    }

    function addRecords($data_to_store) {
        $addRecord = $this->addRecordsCommon($this->tableName, $data_to_store);
        return $addRecord;
    }

    function updateRecords($data_to_update, $key, $value) {
        $updateRecord = $this->updateRecordsCommon($this->tableName, $data_to_update, $key, $value);
        return $updateRecord;
    }

    function getListByIdForOneRecord($key, $value) {
        $recordById = $this->getListByIdForOneRecordCommon($this->tableName, $key, $value, $this->fields);
        return $recordById;
    }

    function getListByIdForAllRecords($key, $value) {
        $recordById = $this->getListByIdForAllRecordsCommon($this->tableName, $key, $value, $this->fields);
        return $recordById;
    }

    function getListByTwoKeysForAllRecords($key1, $value1, $key2, $value2) {
        $recordById = $this->getListByTwoKeysForAllRecordsCommon($this->tableName, $key1, $value1, $key2, $value2, $this->fields);
        return $recordById;
    }

    function deleteAll($ids, $key) {
        $recordById = $this->deleteAllCommon($ids, $this->tableName, $key);
        return $recordById;
    }

}
