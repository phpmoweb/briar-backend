<?php
$mainRole = "";
$text = "Document";
?>
<section class="content-header">
    <h1>
        Add <?=$text?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?=base_url()?>admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?=base_url()?>admin/documents"><?=$text?></a></li>
        <li class="active">Add</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Document Detail</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->

                <form autocomplete="off" role="form" enctype="multipart/form-data" class="validateForm" method="post"
                    action="<?=base_url()?>admin/documents/add">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="doc_for">Document For<span class="kv-reqd">*</span></label>
                                            <select class="form-control" id="doc_for" name="doc_for">
                                                <option value="briar_road_public_school">Briar Road Public School
                                                </option>
                                                <option value="briar_road_preschool">Briar Road Preschool</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="name">Document Title<span class="kv-reqd">*</span></label>
                                            <input type="text" class="form-control" id="categoryName" name="title"
                                                required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="name">Type<span class="kv-reqd">*</span></label>
                                            <select class="form-control" id="type" name="type">
                                                <option value="link">Link</option>
                                                <option value="pdf">Pdf</option>
                                            </select>
                                            <!--<input type="text" class="form-control" id="parentId" name="parentId">-->
                                        </div>
                                    </div>
                                </div>
                                <div class="row" id="documentLink">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="name">Document Link<span class="kv-reqd">*</span></label>
                                            <input type="text" class="form-control" name="documentLink">
                                        </div>
                                    </div>
                                </div>
                                <div class="row" id="documentFile" style="display: none">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="name">Upload Document<span class="kv-reqd">*</span></label>
                                            <input type="file" class="form-control" name="documentPdf">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="box-footer">
                                            <a href="javascript:history.go(-1)" class="btn btn-default">Cancel</a>
                                            <button type="submit" class="submitbtn btn btn-primary">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
</section>
<script>
$(document).ready(function() {
    $('#type').change(function() {
        var type = $(this).val();
        if (type == "pdf") {
            $('#documentFile').show();
            $('#documentLink').hide();
        } else {
            $('#documentLink').show();
            $('#documentFile').hide();
        }
    });
});
</script>