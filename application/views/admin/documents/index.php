<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Documents 
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= base_url('admin/dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a class="active">Documents</a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <a href="<?= base_url() ?>admin/documents/add" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> &nbsp;&nbsp;   Document</a>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <!-- /.box-header -->
                <div class="box-body">

                    <script type="text/javascript">

                        function userSearch() {
                            $('#categoryDataTable').DataTable().destroy();
                            showLoadingBar();
                            var ajaxUrl = "<?php echo base_url() ?>admin/documents/alllist";
                            createDataTable($('#categoryDataTable'), ajaxUrl, "");
                        }

                    </script>
                    <div class="table-responsive">
                        <table id="categoryDataTable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Docoument For</th>
                                    <th>Title</th>
                                    <th>Document</th>
                                    <th>Actions</th> 
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>

                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
</section> 

<script>
    $(document).ready(function () {
        userSearch();
        $('#categoryDataTable').dataTable();
    });
</script>
