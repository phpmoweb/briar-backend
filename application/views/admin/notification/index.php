<?php
$mainRole = "";
?>
<section class="content-header">
    <h1>
        <?= $page_title ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= base_url() ?>admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><?= $page_title ?></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Notification Detail</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->

                <form autocomplete="off" role="form" enctype="multipart/form-data" class="validateForm" method="post" action="<?= base_url() ?>admin/notification/sendNotification">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="name">Send To<span class="kv-reqd">*</span></label>
                                            <select name="os" id="os" class="form-control" required="required">
                                                <option value="">--Select--</option>
                                                <option value="Android">Android</option>
                                                <option value="Iphone">Iphone</option>
                                                <option value="Both">Both</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="name">Notification Message<span class="kv-reqd">*</span></label>
                                            <textarea name="message" id="message" class="form-control" required="required"></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="box-footer">
                                            <a href="javascript:history.go(-1)" class="btn btn-default">Cancel</a>
                                            <button type="submit" class="submitbtn btn btn-primary">Send</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
</section>