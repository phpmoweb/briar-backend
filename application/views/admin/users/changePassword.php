<?php
$text = "Change Password";
?>
<section class="content-header">
    <h1>
        <?= $text ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= base_url() ?>admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?= base_url() ?>admin/users">Users</a></li>
        <li class="active"><?= $text ?></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <!-- /.box-header -->
                <!-- form start -->

                <form autocomplete="off" role="form" enctype="multipart/form-data" class="validateForm" method="post" action="<?= base_url() ?>admin/users/changePassword/<?= $id ?>">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label">Password</label>
                                    <input type="password" name="password" id="forgotPassword" class="form-control" placeholder="Password" required="required">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Confirm Password</label>
                                    <input type="password" name="confirmforgotPassword" class="form-control" placeholder="Confirm Password" required="required">
                                </div>
                                <div class="row">
                                    <!-- /.col -->
                                    <div class="col-xs-12 pull-right">
                                        <button type="submit" class="btn btn-primary btn-block btn-flat">Submit</button>
                                    </div>
                                    <!-- /.col -->
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
</section>