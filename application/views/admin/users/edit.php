<?php
$text = "User";
?>
<section class="content-header">
    <h1>
        Edit <?=$text?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?=base_url()?>admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?=base_url()?>admin/documents"><?=$text?></a></li>
        <li class="active">Edit</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">User Detail</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->

                <form autocomplete="off" role="form" enctype="multipart/form-data" class="validateForm" method="post"
                    action="<?=base_url()?>admin/users/edit/<?= $id ?>">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="firstName">First Name<span class="kv-reqd">*</span></label>
                                            <input type="text" class="form-control" id="firstName" name="firstName" value="<?= $user['firstName'] ?>"
                                                required>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="lastName">Last Name<span class="kv-reqd">*</span></label>
                                            <input type="text" class="form-control" id="lastName" name="lastName" value="<?= $user['lastName'] ?>"
                                                required>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="companyName">School Name<span class="kv-reqd">*</span></label>
                                            <input type="text" class="form-control" id="companyName" name="companyName" value="<?= $user['companyName'] ?>"
                                                required>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="phone">Phone<span class="kv-reqd">*</span></label>
                                            <input type="text" class="form-control" name="phone" value="<?= $user['phone'] ?>" required>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="email">Email<span class="kv-reqd">*</span></label>
                                            <input type="email" class="form-control" name="email" value="<?= $user['email'] ?>" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="password">Password</label>
                                            <input type="password" class="form-control" name="password">
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="box-footer">
                                            <a href="javascript:history.go(-1)" class="btn btn-default">Cancel</a>
                                            <button type="submit" class="submitbtn btn btn-primary">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
</section>