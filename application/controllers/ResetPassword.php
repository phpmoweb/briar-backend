<?php

//require(APPPATH . '/libraries/Custom_Controller.php');

class ResetPassword extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('API/V_1/User_model', 'ci_model');
    }

    function index($token) {
        $data['page_title'] = 'Reset Password';

        $user = $this->ci_model->getListByIdForOneRecord('token', $token);

        if (!empty($user)) {
            if ($user['forgotPasswordExpiredTime'] > date('Y-m-d H:i:s')) {
                $data['id'] = $user['id'];
                $this->load->view('forgotpassword', $data);
            } else {
                $this->load->view('forgotpasswordexpired', $data);
            }
        } else {
            $this->load->view('forgotpasswordexpired', $data);
        }
    }

    function changePassword() {
        if (isset($_POST) && !empty($_POST)) {
            $password = $_POST['forgotPassword'];
            $id = $_POST['id'];

            $params = array(
                'password' => sha1($password),
                'token' => '',
                'forgotPasswordExpiredTime' => ''
            );

            $update = $this->ci_model->updateRecords($params, 'id', $id);

            if ($update) {
                $this->load->view('forgotpasswordchanged');
            } else {
                redirect('resetPassword');
            }
        }
    }

}
