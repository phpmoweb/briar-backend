<?php

error_reporting(0);
require(APPPATH . '/libraries/REST_Controller.php');

class ChangePassword extends REST_Controller {

    function index() {
        if (($this->flag) == 1) {

            $accessToken = $this->accessToken;
            $userId = $this->userId;

            $oldPassword = sha1($_REQUEST['oldPassword']);
            $newPassword = sha1($_REQUEST['newPassword']);

            if (empty($_REQUEST['oldPassword']) || empty($_REQUEST['newPassword'])) {
                $this->setResponseData(STATUS_PARAMETER_MISSING_CODE, "Failure", $accessToken, $this->config->item('parameter_missing'));
            }

            $checkOldPassword = $this->User_model->getListByTwoKeysForAllRecords('id', $userId, 'password', $oldPassword);
            if ($checkOldPassword) {
                $dataToUpdate['password'] = $newPassword;
                $updatePassword = $this->User_model->updateRecords($dataToUpdate, 'id', $userId);
                if ($updatePassword) {
                    $this->setResponseData(STATUS_SUCCESS_CODE, "Success", $accessToken, $this->config->item('password_changed'));
                } else {
                    $this->setResponseData(STATUS_FAILURE_CODE, "Failure", $accessToken, $this->config->item('password_not_changed'));
                }
            } else {
                $this->setResponseData(STATUS_FAILURE_CODE, "Failure", $accessToken, $this->config->item('old_password_not_match'));
            }
        }
    }

}

?>