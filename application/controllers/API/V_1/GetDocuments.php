<?php

/* * **************************************
  Get Documents API Controller
  Created by: Satish Patel
  /*************************************** */

require(APPPATH . '/libraries/REST_Controller.php');

class GetDocuments extends REST_Controller {

    function index() {
        if (($this->flag) == "1") {

            $accessToken = $this->accessToken;
            $versionData = $this->versionData;

            $doc_for = (isset($_POST['doc_for']))?$_POST['doc_for']:"briar_road_public_school";

            $documents = $this->model_name->GetDocuments($doc_for);
            if (!empty($documents)) {
                $this->setResponseData(STATUS_SUCCESS_CODE, "Success", $accessToken, '', $documents);
            } else {
                $this->setResponseData(STATUS_RECORD_NOT_FOUND, "Success", $accessToken, $this->config->item('no_record_found'));
            }
        }
    }

}

?>
