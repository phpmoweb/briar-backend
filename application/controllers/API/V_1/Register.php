<?php

error_reporting(0);
require(APPPATH . '/libraries/REST_Controller.php');

class Register extends REST_Controller {

    function index() {

        if (($this->flag) == 1) {

            $accessToken = $this->accessToken;
            $versionData = $this->versionData;

            if ($this->input->post('firstName')) {
                $data['firstName'] = iconv("Windows-1252", "UTF-8", $this->input->post('firstName'));
            }

            if ($this->input->post('lastName')) {
                $data['lastName'] = iconv("Windows-1252", "UTF-8", $this->input->post('lastName'));
            }
            
            if ($this->input->post('companyName')) {
                $data['companyName'] = iconv("Windows-1252", "UTF-8", $this->input->post('companyName'));
            }

            if ($this->input->post('phone')) {
                $data['phone'] = $this->input->post('phone');
            }

            if ($this->input->post('email')) {
                $data['email'] = $this->input->post('email');
            }
            if ($this->input->post('password')) {
                $data['password'] = sha1($this->input->post('password'));
            }

            $data['createdDate'] = date('Y-m-d H:i:s');
            $data['status'] = 0;

            $fields = 'id';
            $checkUserExists = $this->User_model->getListByIdForOneRecord('email', $data['email']);

            if (empty($checkUserExists)) {
                $user = $this->User_model->addRecords($data);
                if ($user) {
                    $this->setResponseData(STATUS_SUCCESS_CODE, "Success", "", $this->config->item('register_sucess'));
                }
            } else {
                $this->setResponseData(STATUS_FAILURE_CODE, "Failure", "", $this->config->item('email_registered'));
            }
        }
    }

}

?>
