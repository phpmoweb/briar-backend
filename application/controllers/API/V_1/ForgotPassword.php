<?php

error_reporting(0);
require(APPPATH . '/libraries/REST_Controller.php');

class ForgotPassword extends REST_Controller {

    function index() {
        if (($this->flag) == 1) {

            $userId = $this->userId;

            $email = $_REQUEST['email'];
            if (empty($email)) {
                $this->setResponseData(STATUS_PARAMETER_MISSING_CODE, "Failure", "", $this->config->item('parameter_missing'));
            }

            $isEmailExistOrNot = $this->User_model->getListByIdForOneRecord('email', $email);
            if (empty($isEmailExistOrNot)) {
                $this->setResponseData(STATUS_FAILURE_CODE, "Failure", "", $this->config->item('email_not_registered'));
            } else {
                $firstName = $isEmailExistOrNot['firstName'];
                $lastName = $isEmailExistOrNot['lastName'];
                $fullName = $firstName . " " . $lastName;

                $md5email = md5($email);
                $url = base_url() . "resetPassword/index/" . $md5email;
                $sendEmail = $this->sendEmail(APP_NAME . ' - Forgot Password', 'forgotPassword.html', $fullName, $email, $url);
                if ($sendEmail == 1) {
                    $dataToupdate['token'] = $md5email;
                    $dataToupdate['forgotPasswordStatus'] = 'sent';
                    $dataToupdate['forgotPasswordExpiredTime'] = date("Y-m-d H:i:s", strtotime("+1 hours"));
                    $this->User_model->updateRecords($dataToupdate, 'email', $email);
                    $this->setResponseData(STATUS_SUCCESS_CODE, "Success", "", $this->config->item('email_sent'));
                } else {
                    $this->setResponseData(STATUS_FAILURE_CODE, "Failure", "", $this->config->item('email_not_sent'));
                }
            }
        }
    }

}

?>