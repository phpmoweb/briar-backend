<?php

error_reporting(0);

require(APPPATH . '/libraries/REST_Controller.php');

class EditProfile extends REST_Controller {

    function index() {

        if (($this->flag) == 1) {

            $accessToken = $this->accessToken;

            $userId = $this->userId;

            if ($this->input->post('firstName')) {
                $data['firstName'] = iconv("Windows-1252", "UTF-8", $this->input->post('firstName'));
            }

            if ($this->input->post('lastName')) {
                $data['lastName'] = iconv("Windows-1252", "UTF-8", $this->input->post('lastName'));
            }

            if ($this->input->post('phone')) {
                $data['phone'] = $this->input->post('phone');
            }
            
            $data['updatedDate'] = date('Y-m-d H:i:s');

            $updateProfile = $this->User_model->updateRecords($data, 'id', $userId);

            if ($updateProfile) {

                $users = $this->User_model->getListByIdForAllRecords('id', $userId);

                $users[0] = array_map(function($v) {
                    return (is_null($v)) ? "" : $v;
                }, $users[0]);

                $userResponse = array();
                $userResponse = $users;

                $this->setResponseData(STATUS_SUCCESS_CODE, "Success", $accessToken, $this->config->item('profile_success'), $userResponse);
            }
        }
    }

}

?>