<?php

require(APPPATH . '/libraries/Custom_Controller.php');

class Users extends Custom_Controller {

    /**
     * Check if the user is logged in, if he's not, 
     * send him to the login page
     * @return void
     */
    public function __construct() {
        parent::__construct();
        $this->load->model('Users_model');
        $this->ctrname = 'Users';
        $this->controller = 'users';
        $this->key = $this->Users_model->key;
        $this->userData = $this->session->userdata('AdminUserData');
    }

    function index() {
        $data['page_title'] = 'Users';
        $data['content'] = 'admin/' . $this->controller . '/index';
        $this->load->view('admin/layouts/main', $data);
    }

    function alllist() {

        $userData = $this->userData;

// Define Datatables Variables 
        $sLimit = "";
        $start = 0;
        $length = $this->config->item('pagination');
        $draw = 1;

        if (isset($_POST['start']) && $_POST['length'] != '-1') {
            $start = $_POST['start'];
            $length = $_POST['length'];
        }

        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $sWhere = $_POST['search']['value'];

        $dataList = $this->Users_model->get_user_datatables('', $sWhere, $start, $length, 'id', 'DESC');
        $dataListCount = $this->Users_model->get_user_datatables('', $sWhere, '', '', 'id', 'DESC');

        $data = array();

        $no = $_POST['start'];
        foreach ($dataList as $r) {
            $no++;

            if ($r['status'] == "0") {
                $action = '<a title="Click to approve" data-status="1" href="' . base_url('admin/users/changeStatus/' . $r['id']) . '" class="changeStatus btn btn-success btn-xs"><i class="fa fa-check"></i></a>';
            } else {
                $action = '<a title="Click to revoke access" data-status="0" href="' . base_url('admin/users/changeStatus/' . $r['id']) . '" class="changeStatus btn btn-danger btn-xs"><i class="fa fa-times"></i></a>';
            }
            $action .= '&nbsp;<a title="Edit" href="' . base_url('admin/users/edit/' . $r['id']) . '"  class="btn btn-blue btn-xs"><i class="fa fa-edit"></i></a>';
            $action .= '&nbsp;<a title="Delete" data-href="' . base_url('admin/users/delete/' . $r['id']) . '" data-msg="Do you want to delete this user account?" class="delete btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>';
            $action .= '&nbsp;<a title="Change Password" href="' . base_url('admin/users/changePassword/' . $r['id']) . '"  class="btn btn-primary btn-xs"><i class="fa fa-key"></i></a>';

            $row = array();
            $row[] = $r['firstName'] . ' ' . $r['lastName'];
            $row[] = $r['email'];
            $row[] = $r['phone'];
            $row[] = $r['companyName'];
            $row[] = ($r['status'] == "1") ? "<span class='text-success'>Approved</span>" : "<span class='text-danger'>Revoked</spann>";
            $row[] = $action;
            $data[] = $row;
        }
        $output = array(
            "draw" => $draw,
            "recordsTotal" => intval(count($dataListCount)),
            "recordsFiltered" => intval(count($dataListCount)),
            "data" => $data
        );

        echo json_encode($output);
        exit();
    }

    function add() {
        $data['page_title'] = 'Add User';
        $error = "";
        if (isset($_POST) && !empty($_POST)) {

            $emailExit = $this->Users_model->checkUserEmailExist($_POST['email']);
            if(!empty($emailExit)){
                $this->session->set_flashdata('flash_message_success', $this->config->item('user_email_exist'));
                redirect('admin/' . $this->controller.'/add');
            }

            $data_to_store = $_POST;
            $data_to_store['password'] = sha1($_POST['password']);

            $addRecord = $this->Users_model->addRecords($data_to_store);
            if (!empty($addRecord)) {
                $this->session->set_flashdata('flash_message_success', $this->config->item('record_added'));
            }

            redirect('admin/' . $this->controller);
        }
        $data['content'] = 'admin/' . $this->controller . '/add';
        $this->load->view('admin/layouts/main', $data);
    }

    function edit($id) {
        $data['page_title'] = 'Edit User';
        $error = "";
        $data['user'] = $this->Users_model->getUser($id);
        $data['id'] = $id;
        if (isset($_POST) && !empty($_POST)) {
            $emailExit = $this->Users_model->checkUserEmailExist($_POST['email'],$id);
            if(!empty($emailExit)){
                $this->session->set_flashdata('flash_message_success', $this->config->item('user_email_exist'));
                redirect('admin/' . $this->controller.'/edit/'.$id);
            }

            $data_to_store = $_POST;
            if(isset($_POST['password']) && $_POST['password'] != ""){
                $data_to_store['password'] = sha1($_POST['password']);
            }else{
                $data_to_store['password'] = $data['user']['password'];
            }

            $addRecord = $this->Users_model->updateRecords($data_to_store,'id',$id);
            if (!empty($addRecord)) {
                $this->session->set_flashdata('flash_message_success', $this->config->item('record_updated'));
            }

            redirect('admin/' . $this->controller);
        }
        $data['content'] = 'admin/' . $this->controller . '/edit';
        $this->load->view('admin/layouts/main', $data);
    }

    function changePassword($id) {
        $data['page_title'] = 'Edit User';
        $data['id'] = $id;

        if (isset($_POST) && !empty($_POST)) {

            $password = $_REQUEST['password'];

            $data_to_update = array(
                'password' => sha1($password)
            );

            $updateRecord = $this->Users_model->updateRecords($data_to_update, $this->key, $id);

            $this->session->set_flashdata('flash_message_success', $this->config->item('record_updated'));
            redirect(base_url('admin/') . $this->controller);
        }

        $data['content'] = 'admin/' . $this->controller . '/changePassword';
        $this->load->view('admin/layouts/main', $data);
    }

    function delete($id) {
        if ($id) {
            $key = 'id';
            $result = $this->Users_model->deleteAll($id, $key);
        }
        if ($result == '1') {
            $this->session->set_flashdata('flash_message_success', $this->config->item('user_delete'));
        }
        redirect(base_url() . 'admin/' . $this->controller);
    }

    function changeStatus($id) {
        $status = $_REQUEST['status'];
        $data_to_update['status'] = $status;
        $updateRecord = $this->Users_model->updateRecords($data_to_update, $this->key, $id);
        if ($updateRecord) {
            if ($status == 1) {
                $this->session->set_flashdata('flash_message_success', $this->config->item('approve_user'));
            } else {
                $this->session->set_flashdata('flash_message_success', $this->config->item('inactive_user'));
            }
        }
        redirect(base_url() . 'admin/' . $this->controller);
    }

    function emailExists($email) {
        $this->db->where('emailId', $email);
        $query = $this->db->get('sos_users');
        if ($query->num_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function registerEmailExists() {
        if (array_key_exists('emailId', $_POST)) {
            if ($this->emailExists($this->input->post('emailId')) == TRUE) {
                echo json_encode(FALSE);
            } else {
                echo json_encode(TRUE);
            }
        }
    }

    function phoneExists($phoneNumber) {
        $this->db->where('phoneNumber', $phoneNumber);
        $query = $this->db->get('sos_users');
        if ($query->num_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function registerPhoneExists() {
        if (array_key_exists('phoneNumber', $_POST)) {
            if ($this->phoneExists($this->input->post('phoneNumber')) == TRUE) {
                echo json_encode(FALSE);
            } else {
                echo json_encode(TRUE);
            }
        }
    }

}

?>