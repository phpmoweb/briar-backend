<?php

require(APPPATH . '/libraries/Custom_Controller.php');

class Documents extends Custom_Controller {

    /**
     * Check if the user is logged in, if he's not, 
     * send him to the login page
     * @return void
     */
    public function __construct() {
        parent::__construct();
        $this->load->model('Documents_model');
        $this->ctrname = 'Documents';
        $this->controller = 'documents';
        $this->key = $this->Documents_model->key;
        $this->userData = $this->session->userdata('AdminUserData');
    }

    function index() {
        $data['page_title'] = 'Documents';
        $data['content'] = 'admin/' . $this->controller . '/index';
        $this->load->view('admin/layouts/main', $data);
    }

    function alllist() {

        $userData = $this->userData;

        $sLimit = "";
        $start = 0;
        $length = $this->config->item('pagination');
        $draw = 1;

        if (isset($_POST['start']) && $_POST['length'] != '-1') {
            $start = $_POST['start'];
            $length = $_POST['length'];
        }

        if (isset($_POST['draw'])) {
            $draw = $_POST['draw'];
        }

        $sWhere = $_POST['search']['value'];

        $dataList = $this->Documents_model->get_document_datatables('', $sWhere, $start, $length, 'title', 'ASC');
        $dataListCount = $this->Documents_model->get_document_datatables('', $sWhere, '', '', 'title', 'ASC');

        $data = array();

        $no = $_POST['start'];
        foreach ($dataList as $r) {
            $no++;

            $action = '<a href="' . base_url('admin/' . $this->controller . '/edit/' . $r['id']) . '" class="btn btn-primary btn-xs"><i class="fa fa-edit"></i></a>&nbsp;';
            $action .= '<a data-href="' . base_url('admin/' . $this->controller . '/delete/' . $r['id']) . '" class="delete btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>&nbsp;';

            if ($r['type'] == "link") {
                $document = '<a href="' . $r['document'] . '" target="_blank"><i class="fa fa-link fa-2x"></i></a>';
            } else {
                $document = '<a href="' . base_url('uploads/documents/' . $r['document']) . '" target="_blank"><i class="fa fa-file-pdf-o fa-2x"></i></a>';
            }

            $row = array();
            $row[] = ($r['doc_for'] == "briar_road_public_school")?"Briar Road Public School":"Briar Road Preschool";
            $row[] = $r['title'];
            $row[] = $document;
            $row[] = $action;
            $data[] = $row;
        }
        $output = array(
            "draw" => $draw,
            "recordsTotal" => intval(count($dataListCount)),
            "recordsFiltered" => intval(count($dataListCount)),
            "data" => $data
        );

        echo json_encode($output);
        exit();
    }

    function add() {
        $data['page_title'] = 'Add Document';
        $error = "";
        if (isset($_POST) && !empty($_POST)) {
            $data_to_store['doc_for'] = $this->input->post('doc_for');
            $data_to_store['title'] = $this->input->post('title');
            $data_to_store['type'] = $this->input->post('type');
            if ($this->input->post('type') == "pdf") {
                if (!empty($_FILES['documentPdf']) && $_FILES['documentPdf']['name'] != "") {
                    $config = array(
                        'upload_path' => realpath('./uploads/documents/'),
                        'field' => 'documentPdf',
                        'allowed_types' => 'pdf',
                        'max_size' => '30000',
                    );

                    $response = $this->uploadFile($config);
                    if ($response['status'] == "success") {
                        $data_to_store['document'] = $response['name'];
                    } else {
                        $this->session->set_flashdata('flash_message_error', $response['msg']);
                        redirect('admin/' . $this->controller . '/add');
                    }
                }
            } else {
                $data_to_store['document'] = $this->input->post('documentLink');
            }

            $addRecord = $this->Documents_model->addRecords($data_to_store);
            if (!empty($addRecord)) {

                $this->session->set_flashdata('flash_message_success', $this->config->item('record_added'));
            }

            redirect('admin/' . $this->controller);
        }
        $data['content'] = 'admin/' . $this->controller . '/add';
        $this->load->view('admin/layouts/main', $data);
    }

    function edit($id) {
        $recordById = $this->Documents_model->getListByIdForOneRecord('id', $id);
        $memberdata['page_title'] = 'Edit Document';
        $memberdata['recordById'] = $recordById;
        $memberdata['content'] = 'admin/' . $this->controller . '/edit';
        $memberdata['id'] = $id;
        $this->load->view('admin/layouts/main', $memberdata);

        if ($this->input->server('REQUEST_METHOD') === 'POST') {

            $data_to_update['title'] = $this->input->post('title');
            $data_to_update['doc_for'] = $this->input->post('doc_for');
            $data_to_update['type'] = $this->input->post('type');
            if ($this->input->post('type') == "pdf") {
                if (!empty($_FILES['documentPdf']) && $_FILES['documentPdf']['name'] != "") {
                    $config = array(
                        'upload_path' => realpath('./uploads/documents/'),
                        'field' => 'documentPdf',
                        'allowed_types' => 'pdf',
                        'max_size' => '30000',
                    );

                    $response = $this->uploadFile($config);
                    if ($response['status'] == "success") {
                        $data_to_update['document'] = $response['name'];
                    } else {
                        $this->session->set_flashdata('flash_message_error', $response['msg']);
                        redirect('admin/' . $this->controller . '/edit/' . $id);
                    }
                }
            } else {
                $data_to_update['document'] = $this->input->post('documentLink');
            }

            $updateRecord = $this->Documents_model->updateRecords($data_to_update, $this->key, $id);
            if (!empty($updateRecord)) {
                $this->session->set_flashdata('flash_message_success', $this->config->item('record_updated'));
            }

            redirect(base_url() . 'admin/' . $this->controller);
        }
    }

    function delete($id) {

        $key = $this->key;
        $result = $this->Documents_model->deleteAll($id, $key);

        if ($result == '1') {
            $this->session->set_flashdata('flash_message_success', $this->config->item('record_delete'));
        }
        redirect(base_url() . 'admin/' . $this->controller);
    }

    function changeStatus($id) {
        $status = $_REQUEST['status'];
        $data_to_update['status'] = $status;
        $updateRecord = $this->Users_model->updateRecords($data_to_update, $this->key, $id);
        if ($updateRecord) {
            if ($status == 1) {
                $this->session->set_flashdata('flash_message_success', $this->config->item('approve_user'));
            } else {
                $this->session->set_flashdata('flash_message_success', $this->config->item('inactive_user'));
            }
        }
        redirect(base_url() . 'admin/' . $this->controller);
    }

    function emailExists($email) {
        $this->db->where('emailId', $email);
        $query = $this->db->get('sos_users');
        if ($query->num_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function registerEmailExists() {
        if (array_key_exists('emailId', $_POST)) {
            if ($this->emailExists($this->input->post('emailId')) == TRUE) {
                echo json_encode(FALSE);
            } else {
                echo json_encode(TRUE);
            }
        }
    }

    function phoneExists($phoneNumber) {
        $this->db->where('phoneNumber', $phoneNumber);
        $query = $this->db->get('sos_users');
        if ($query->num_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function registerPhoneExists() {
        if (array_key_exists('phoneNumber', $_POST)) {
            if ($this->phoneExists($this->input->post('phoneNumber')) == TRUE) {
                echo json_encode(FALSE);
            } else {
                echo json_encode(TRUE);
            }
        }
    }

}

?>