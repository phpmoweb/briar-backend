<?php

require(APPPATH . '/libraries/Custom_Controller.php');

class Notification extends Custom_Controller {

    /**
     * Check if the user is logged in, if he's not, 
     * send him to the login page
     * @return void
     */
    public function __construct() {
        parent::__construct();
        $this->load->model('Notification_model');
        $this->ctrname = 'Notification';
        $this->controller = 'notification';
        $this->key = $this->Notification_model->key;
        $this->userData = $this->session->userdata('AdminUserData');
    }

    function index() {
        $data['page_title'] = 'Send Notification';
        $data['content'] = 'admin/' . $this->controller . '/index';
        $this->load->view('admin/layouts/main', $data);
    }

    function sendNotification() {
        if (isset($_POST) && !empty($_POST)) {
            $text_msg = $_POST['message'];
            $os = $_POST['os'];

            /* send notification to ios */
            $devices = $this->Notification_model->getDevices($os);
            if (!empty($devices)) {

                for ($i = 0; $i < sizeof($devices); $i++) {

                    $token = $devices[$i]['deviceToken'];

                    $response = $this->Notification_model->sendNotification($devices[$i]['deviceType'], $text_msg, $token);

                    if ($os == "Android") {
                        $log = array(
                            'userId' => $devices[$i]['userId'],
                            'response' => $response
                        );

                        $this->db->insert('tbl_notification_logs', $log);
                    }
                }
            }

            $data_to_store = array();
            $data_to_store['deviceType'] = $os;
            $data_to_store['message'] = $text_msg;
            $data_to_store['createdDate'] = date('Y-m-d H:i:s');

            $addRecord = $this->Notification_model->addRecords($data_to_store);

            if (!empty($addRecord)) {
                $this->session->set_flashdata('flash_message_success', $this->config->item('notification_sent'));
            }

            redirect('admin/' . $this->controller);
        }
    }

}

?>